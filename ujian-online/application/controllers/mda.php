<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Singapore");
class Mda extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+8:00'");
        $waktu_sql = $this->db->query("SELECT NOW() AS waktu")->row_array();
        $this->waktu_sql = $waktu_sql['waktu'];
        $this->opsi = array("a","b","c","d","e");
	}

	function index(){
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		$a['p']			= "v_main_b";

		$this->load->view('bbb', $a);
	}

	public function cek_aktif() {

		if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
			redirect('adm/login');
		} 
	}

	public function m_soal_sp() {
		$this->cek_aktif();
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');

		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);

		if ($uri3 == "edit") {
			
			
			if ($uri4 == 0) {
				// $a['d'] = array("mode"=>"add","id"=>"0","id_guru"=>$id_guru,"id_mapel"=>"","bobot"=>"1","file"=>"","soal"=>"","opsi_a"=>"#####","opsi_b"=>"#####","opsi_c"=>"#####","opsi_d"=>"#####","opsi_e"=>"#####","jawaban"=>"","tgl_input"=>"");
			} else {
				// $a['d'] = $this->db->query("SELECT m_soal.*, 'edit' AS mode FROM m_soal WHERE id = '$uri4'")->row_array();

			}

			$data = array();

		


			$a['data_pc'] = $data;
			$a['p'] = "f_soal_bbb";

		}else if($uri3 == "hapus"){
			
			$this->db->query("DELETE FROM sp_master WHERE id = '".$uri4."'");
			$this->db->query("DELETE FROM sp_child WHERE id_sp_master = '".$uri4."'");
			
			redirect('mda/m_soal_sp');
		}else if ($uri3 == "simpan") {
			$p = $this->input->post();
			
			$arg = explode(",", $p['deret']);
			$n = count($arg);

			$k = 1;
			for ($i=1; $i <= $n ; $i++) { 
				$k = $k * $i;
			}


			$pdata = array(
				"deret"=>$p['deret'],
				"jumlah"=>$k
			);

			$this->db->insert("sp_master", $pdata);
			$get_id_akhir = $this->db->query("SELECT MAX(id) maks FROM sp_master LIMIT 1")->row_array();
			$__id_soal = $get_id_akhir['maks'];
			
			$this->generatePermutasi($p['deret'],$__id_soal);
			
			redirect('mda/m_soal_sp');
		}else if ($uri3 == "data") {
				$start = $this->input->post('start');
		        $length = $this->input->post('length');
		        $draw = $this->input->post('draw');
		        $search = $this->input->post('search');

		        
		        $d_total_row = $this->db->query("SELECT a.* FROM sp_master a")->num_rows();

		        $q_datanya = $this->db->query("SELECT a.* FROM sp_master a ORDER BY a.id DESC LIMIT ".$start.", ".$length)->result_array();
		        
		        $data = array();
		        $no = ($start+1);

		        foreach ($q_datanya as $d) {

		        	$xx = explode(',', $d['deret']);
		        	$jl = '';
					foreach ($xx as $ke) {
					$jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
					}

		            $data_ok = array();
		            $data_ok[0] = $no++;
		            $data_ok[1] = $jl;
		            $data_ok[2] = '<div class="btn-group">
	                          <a href="'.base_url().'mda/m_soal_sp/det/'.$d['id'].'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-search" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Lihat</a>
	                          <a href="'.base_url().'mda/m_soal_sp/hapus/'.$d['id'].'" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
	                         ';

		            $data[] = $data_ok;
		        }

		        $json_data = array(
		                    "draw" => $draw,
		                    "iTotalRecords" => $d_total_row,
		                    "iTotalDisplayRecords" => $d_total_row,
		                    "data" => $data
		                );
		        j($json_data);
		        exit;
		}else if ($uri3 == "data_det") {
				$start = $this->input->post('start');
		        $length = $this->input->post('length');
		        $draw = $this->input->post('draw');
		        $search = $this->input->post('search');

		        
		        $d_total_row = $this->db->query("SELECT a.* FROM sp_child a WHERE a.id_sp_master = '".$uri4."' ")->num_rows();

		        $q_datanya = $this->db->query("SELECT a.* FROM sp_child a WHERE a.id_sp_master = '".$uri4."' ORDER BY RAND() LIMIT ".$start.", ".$length)->result_array();
		        
		        $data = array();
		        $no = ($start+1);

		        foreach ($q_datanya as $d) {
		        	$xx = explode(',', $d['acak']);
		        	$jl = '';
					foreach ($xx as $ke) {
					$jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
					}

		            $data_ok = array();
		            $data_ok[0] = $no++;

					

		            $data_ok[1] = $jl;
		            $data_ok[2] = $d['jawaban'];
		            

		            $data[] = $data_ok;
		        }

		        $json_data = array(
		                    "draw" => $draw,
		                    "iTotalRecords" => $d_total_row,
		                    "iTotalDisplayRecords" => $d_total_row,
		                    "data" => $data
		                );
		        j($json_data);
		        exit;
		}else if ($uri3 == "det") {
			
			$a['sp_master'] = $this->db->query("SELECT * FROM sp_master WHERE id = '".$uri4."'")->row();
			// $a['sp_child'] = $this->db->query("SELECT * FROM sp_child WHERE id_sp_master = '$uri4'")->row();
			//$a['hasil'] = $this->db->query("")->result();
			// $a['p'] = "m_guru_tes_hasil_detil";
			$a['p'] = "m_det_bbb";
		}else{
			$a['p']	= "m_soal_bbb";
		}

		
		$this->load->view('bbb', $a);
	}

	function generatePermutasi($str,$id){
		// $str = "KRZST";
		//$n = strlen($str);
		//$this->permute($str, 0, $n - 1,$id);

		//$str = "M,K,L,N,W";
		// $n = strlen($str);
		$arg = explode(",", $str);
		$n = count($arg);

		$this->permute($arg, 0, $n - 1,$id);
	}

	function permute($arg, $l, $r,$id)
	{

		if ($l == $r){

			$hilang = end($arg);
			$new = array_diff($arg, array($hilang));

			$pdata = array(
				"id_sp_master"=>$id,
				"acak"=>implode(',', $new),
				"jawaban"=>$hilang,
			);
			$this->db->insert("sp_child", $pdata);
		}
		else
		{
			for ($i = $l; $i <= $r; $i++)
			{
				$arg = $this->swap($arg, $l, $i);
				$this->permute($arg, $l + 1, $r,$id);
				$arg = $this->swap($arg, $l, $i);
			}
		}
	}

	/* Swap Characters at position @param
	a string value @param i position 1
	@param j position 2 @return swapped
	string */
	function swap($charArray, $i, $j)
	{
		$temp;
		//$charArray = str_split($a);
		//$charArray = implode(',', $a);
		$temp = $charArray[$i] ;
		$charArray[$i] = $charArray[$j];
		$charArray[$j] = $temp;
		return $charArray;
	}

	public function m_ujian_sp() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));

		if ($uri3 == "det") {
			$are = array();

			$a = $this->db->query("SELECT * FROM tr_psikotest WHERE id = '$uri4'")->row();
			
			if (!empty($a)) {
				$pc_waktu = explode(" ", $a->tgl_mulai);
				$pc_tgl = explode("-", $pc_waktu[0]);
				
				$ps_waktu = explode(" ", $a->show_result);
				$ps_tgl = explode("-", $ps_waktu[0]);

				$are['id'] = $a->id;
				$are['nama_ujian'] = $a->nama_ujian;
				$are['jumlah_kolom'] = $a->jumlah_kolom;
				$are['jumlah_perkolom'] = $a->jumlah_perkolom;
				$are['waktu'] = $a->waktu;
				$are['terlambat'] = $a->terlambat;
				$are['tgl_mulai'] = $pc_waktu[0];
				$are['wkt_mulai'] = substr($pc_waktu[1],0,5);
				$are['token'] = $a->token;
				$are['tgl_show'] = $ps_waktu[0];
				$are['wkt_show'] = substr($ps_waktu[1],0,5);
			} else {
				$are['id'] = "";
				$are['nama_ujian'] = "";
				$are['jumlah_kolom'] = "";
				$are['jumlah_perkolom'] = "";
				$are['waktu'] = "";
				$are['terlambat'] = "";
				$are['tgl_mulai'] = "";
				$are['wkt_mulai'] = "";
				$are['token'] = "";
				$are['tgl_show'] = "";
				$are['wkt_show'] = "";
			}

			j($are);
			exit();
		}else if ($uri3 == "simpan") {
			$ket 	= "";
			
			if ($p->id != 0) {
				// $this->db->query("UPDATE tr_guru_tes SET id_mapel = '".bersih($p,"mapel")."', 
				// 				nama_ujian = '".bersih($p,"nama_ujian")."', jumlah_soal = '".bersih($p,"jumlah_soal")."', 
				// 				waktu = '".bersih($p,"waktu")."', terlambat = '".bersih($p,"terlambat")."', 
				// 				tgl_mulai = '".bersih($p,"tgl_mulai")." ".bersih($p,"wkt_mulai")."', show_result = '".bersih($p,"tgl_show")." ".bersih($p,"wkt_show")."', jenis = '".bersih($p,"acak")."', passing_grade = '".bersih($p,"passing_grade")."'
				// 				WHERE id = '".bersih($p,"id")."'");\

				$this->db->query("UPDATE tr_psikotest SET nama_ujian = '".bersih($p,"nama_ujian")."',jumlah_perkolom = '".bersih($p,"jumlah_perkolom")."', waktu = '".bersih($p,"waktu")."', terlambat = '".bersih($p,"terlambat")."',tgl_mulai = '".bersih($p,"tgl_mulai")." ".bersih($p,"wkt_mulai")."', show_result = '".bersih($p,"tgl_show")." ".bersih($p,"wkt_show")."' WHERE id = '".bersih($p,"id")."'");

				
				$ket = "edit";
			} else {
				$ket = "tambah";
				$token = strtoupper(random_string('alpha', 5));

				// $this->db->query("INSERT INTO tr_guru_tes VALUES (null, '".$a['sess_konid']."', '".bersih($p,"mapel")."',
				// 				'".bersih($p,"nama_ujian")."', '".bersih($p,"jumlah_soal")."', '".bersih($p,"waktu")."', '".bersih($p,"acak")."', 
				// 				'', '".bersih($p,"tgl_mulai")." ".bersih($p,"wkt_mulai")."', '".
				// 				bersih($p,"terlambat")."', '$token', '".bersih($p,"passing_grade")."', '".bersih($p,"tgl_show")." ".bersih($p,"wkt_show")."')");
				$kk = bersih($p,"plh");
				$jmlkolom = count($kk);
 				
				$this->db->query("INSERT INTO tr_psikotest VALUES (null,'".bersih($p,"nama_ujian")."','".serialize(bersih($p,"plh"))."','".$jmlkolom."','".bersih($p,"jumlah_perkolom")."', '".bersih($p,"waktu")."', '".bersih($p,"tgl_mulai")." ".bersih($p,"wkt_mulai")."', '".bersih($p,"terlambat")."', '$token', '".bersih($p,"tgl_show")." ".bersih($p,"wkt_show")."')");
			}
			

			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= $ket." sukses";
			j($ret_arr);
			exit();
		}else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT id FROM tr_psikotest a WHERE a.nama_ujian LIKE '%".$search['value']."%'")->num_rows();
	    
	        $q_datanya = $this->db->query("SELECT a.* FROM tr_psikotest a WHERE a.nama_ujian LIKE '%".$search['value']."%' ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();
	        $data = array();
	        $no = ($start+1);

	        foreach ($q_datanya as $d) {
	        	$gn = '';
	        	foreach(unserialize($d['deret']) as $vb){
	        		$xx = explode(',', $vb);
		        	$jl = '';
						foreach ($xx as $ke) {
						$jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
					}

	        		$gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
	        	}
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['nama_ujian']."<br>Token : <b>".$d['token']."</b> &nbsp;&nbsp; <a href='#' onclick='return refresh_tokenbbb(".$d['id'].")' title='Perbarui Token'><i class='fa fa-refresh'></i></a>";
	            $data_ok[2] = $gn;
	            $data_ok[3] = $d['jumlah_kolom'];
	            $data_ok[4] = $d['jumlah_perkolom'];
	            $data_ok[5] = tjs($d['tgl_mulai'],"s")."<br>(".$d['waktu']." menit)";
	            $data_ok[6] = '
	            	<div class="btn-group">
                      <a href="#" onclick="return m_ujian_ebbb('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                      <a href="#" onclick="return m_ujian_hbbb('.$d['id'].');" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                    </div>
                         ';

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		}else if ($uri3 == "hapus") {
			$this->db->query("DELETE FROM tr_psikotest WHERE id = '".$uri4."'");
			$ret_arr['status'] 	= "ok";
			$ret_arr['caption']	= "hapus sukses";
			j($ret_arr);
			exit();
		}else if ($uri3 == "refresh_token") {
			$token = strtoupper(random_string('alpha', 5));

			$this->db->query("UPDATE tr_psikotest SET token = '$token' WHERE id = '$uri4'");

			$ret_arr['status'] = "ok";
			j($ret_arr);
			exit();
		}else {


			$a['data'] = $this->db->query("SELECT * FROM sp_master a")->result();
			$a['p']	= "m_guru_tes_bbb";
		}

		
		$this->load->view('bbb', $a);
	}

	public function ikuti_ujian_sp() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));
		//return as json
		$jeson = array();
		//$a['sess_konid']
		$a['data'] = $this->db->query("SELECT 
									a.id, a.nama_ujian, a.jumlah_kolom, a.waktu,
									IF((d.status='Y' AND NOW() BETWEEN d.tgl_mulai AND d.tgl_selesai),'Sedang Tes',
									IF(d.status='Y' AND NOW() NOT BETWEEN d.tgl_mulai AND d.tgl_selesai,'Waktu Habis',
									IF(d.status='N','Selesai','Belum Ikut'))) status 
									FROM tr_psikotest a
									LEFT JOIN tr_ikut_psikotest d ON CONCAT('".$a['sess_konid']."',a.id) = CONCAT(d.id_user,d.id_psikotest)
									ORDER BY a.id ASC")->result();
		//echo $this->db->last_query();
		$a['p']	= "m_list_ujian_siswa_bbb";
		$this->load->view('bbb', $a);
	}

	public function ikut_ujian_sp() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));

		if ($uri3 == "simpan_satu") {
			$p			= json_decode(file_get_contents('php://input'));
			// echo $uri4.'-'.$a['sess_konid'].'-'.$p->plh.'-'.$p->ch.'-'.$p->ms;
			
			$nm_ms_plh = str_replace("_",",",$p->ms);
			$nm_ch_plh = str_replace("_",",",$p->ch);

			$q_cek_sdh_ujian= $this->db->query("SELECT * FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'");
			$d_cek_sdh_ujian= $q_cek_sdh_ujian->row();
			

			$time_updated = date('H:i:s');
			$intbl = $d_cek_sdh_ujian->time_updated;

			$time1 = new DateTime($time_updated);
			$time2 = new DateTime(date('H:i:s',strtotime($intbl)));
			$interval = $time1->diff($time2);
			$nn = $interval->format('%H:%I:%s');

			$kumpulan = array();
			$bigkumpulan = array();

			$hh = unserialize($d_cek_sdh_ujian->list_soal);
			$inv = 0;
			foreach($hh as $gth) {
			 	$kumpulan[$inv]['master'] = $gth['master'];
			 	$ghh = array();
			 	foreach ($gth['chil'] as $vls) {

			// 		// if($nm_ms_plh==$gth['master']){
			// 		// 	if($vls['acak']==$nm_ch_plh){
			// 		// 		$gh['acak'] = $vls['acak'];
			// 		// 		$gh['jbwn'] = $p->plh;
			// 		// 		$gh['tm'] = 0;
			// 		// 	}						
			// 		// }else{
			// 		// 	$gh['acak'] = $vls['acak'];						
			// 		// }
			 		$gh['acak'] = $vls['acak'];

					if($nm_ms_plh==$gth['master']){
					 	if($vls['acak']==$nm_ch_plh){
					 		$gh['jbwn'] = $p->plh;
					 		$gh['tm'] = $nn;
						}else{
							$gh['jbwn'] = $vls['jbwn'];
							$gh['tm'] = $vls['tm'];
						}
					}else{
						$gh['jbwn'] = $vls['jbwn'];
						$gh['tm'] = $vls['tm'];
					}
			 		
			 		
			 		$ghh [] = $gh;
			 	}
			 	$kumpulan[$inv]['chil'] = $ghh;
			 	$inv++;
			}


		
			$list_soal = serialize($kumpulan);

			
			// $list_soal = serialize($bigkumpulan);

			$this->db->query("UPDATE tr_ikut_psikotest SET list_soal = '".$list_soal."', time_updated = '".$time_updated."' WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'");

			$d['status'] = "ok";

			j($d);
			exit;		

		} else if ($uri3 == "simpan_akhir") {
			$p			= json_decode(file_get_contents('php://input'));
			$are = $this->db->query("SELECT tr_ikut_psikotest.* FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'")->row();
			$soal = unserialize($are->list_soal);
        	$bnr = 0;
        	$slh = 0;
        	$ksg = 0;
        	$tim = array();
			foreach ($soal as $d) { 
				$hd = explode(',', $d['master']);
				$jmlhead = count($hd);
				
                foreach ($d['chil'] as $vls) {
                	
                	$master_ps = $this->db->query("SELECT * FROM sp_master WHERE deret = '".$d['master']."'")->row();
                    $child_ps = $this->db->query("SELECT * FROM sp_child WHERE id_sp_master = '".$master_ps->id."' and acak='".$vls['acak']."'")->row();
                    if($vls['jbwn']=="0"){
                    	$ksg++;
                    }else{
                    	$tim[] = date("H:i:s",strtotime($vls['tm']));
                    	if($vls['jbwn']==$child_ps->jawaban)
                        {
                        	$bnr++; 
                        }else{ 
                          	$slh++;
                        }
                    }
                }
			}

			$rata_a = date('H:i:s', array_sum(array_map('strtotime', $tim)) / count($tim)).'<br>';
      		$tertinggi_a = max($tim);
      		$terendah_a = min($tim);
			
			$this->db->query("UPDATE tr_ikut_psikotest SET status = 'N', jumlah_benar = '$bnr', jumlah_salah = '$slh', jumlah_non = '$ksg', time_tertinggi = '$tertinggi_a', time_terendah = '$terendah_a', time_rata = '$rata_a' WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'");
			$a['status'] = "ok";
			j($a);
			exit;		
		} else if ($uri3 == "token") {
			$a['du'] = $this->db->query("SELECT a.id, a.tgl_mulai, a.terlambat, a.token, a.nama_ujian, a.jumlah_kolom, a.jumlah_perkolom, a.waktu FROM tr_psikotest a WHERE a.id = '$uri4'")->row_array();

			$a['dp'] = $this->db->query("SELECT * FROM m_siswa WHERE id = '".$a['sess_konid']."'")->row_array();

			if (!empty($a['du']) || !empty($a['dp'])) {
				$tgl_selesai = $a['du']['tgl_mulai'];
			    $tgl_selesai = strtotime($tgl_selesai);
			    $tgl_baru = date('F j, Y H:i:s', $tgl_selesai);

			    $tgl_terlambat = strtotime("+".$a['du']['terlambat']." minutes", $tgl_selesai);	
				$tgl_terlambat_baru = date('F j, Y H:i:s', $tgl_terlambat);

				$a['tgl_mulai'] = $tgl_baru;
				$a['terlambat'] = $tgl_terlambat_baru;

				$a['p']	= "m_token_bbb";
				$this->load->view('bbb', $a);
			} else {
				redirect('mda/ikuti_ujian_sp');
			}
		}else{
			$cek_sdh_selesai= $this->db->query("SELECT id FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."' AND status = 'N'")->num_rows();
			
			//sekalian validasi waktu sudah berlalu...
			if ($cek_sdh_selesai < 1) {
				//ini jika ujian belum tercatat, belum ikut
				//ambil detil soal
				$cek_detil_tes = $this->db->query("SELECT * FROM tr_psikotest WHERE id = '$uri4'")->row();
				$q_cek_sdh_ujian= $this->db->query("SELECT id FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'");
				$d_cek_sdh_ujian= $q_cek_sdh_ujian->row();
				$cek_sdh_ujian	= $q_cek_sdh_ujian->num_rows();
				$acakan = "ORDER BY RAND()";
				// header('Content-Type: application/json; charset=utf-8');

				//blm ujian
				if ($cek_sdh_ujian < 1)	{
					$waktu_selesai = tambah_jam_sql($cek_detil_tes->waktu);					
					$time_mulai		= date('Y-m-d H:i:s');
					$time_updated		= date('H:i:s');
					$drt = unserialize($cek_detil_tes->deret);
					$kumpulan = array();
					$bigkumpulan = array();
					$inv = 0;
					foreach ($drt as $kue) {

						$master_ps = $this->db->query("SELECT * FROM sp_master WHERE deret = '$kue'")->row();
						//echo $master_ps->id.' '.$master_ps->deret.' '.$master_ps->jumlah.'<br>';
						$kumpulan[$inv]['master'] = $master_ps->deret;
						
						$child_ps = $this->db->query("SELECT a.* FROM sp_child a WHERE a.id_sp_master = '".$master_ps->id."' ORDER BY RAND()")->result_array();
						
						$chl = array();
						foreach ($child_ps as $lue) {
							$chl[] = $lue['acak'];
						}
						shuffle($chl);
						shuffle($chl);
						$ghh = array();
						
						for ($mm=0; $mm < $cek_detil_tes->jumlah_perkolom ; $mm++) { 
							//$gh['mst'] = $master_ps->deret;
							$gh['acak'] = $chl[$mm];
							$gh['jbwn'] = 0;
							$gh['tm'] = 0;
							$ghh [] = $gh;
						}

						
						$kumpulan[$inv]['chil'] = $ghh;
						// $bigkumpulan[] = $kumpulan;
						$inv++;
					}
					
					$list_soal = serialize($kumpulan);

					$qsl = "INSERT INTO tr_ikut_psikotest VALUES (null, '$uri4', '".$list_soal."', '".$a['sess_konid']."', '$time_mulai', ADDTIME('$time_mulai', '$waktu_selesai'), null,null,null, null,null,null,'$time_updated', 'Y')";
				
					$this->db->query($qsl);
					
					$detil_tes = $this->db->query("SELECT * FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'")->row();
				} else {
					//udah ujian
					$q_ambil_soal 	= $this->db->query("SELECT * FROM tr_ikut_psikotest WHERE id_psikotest = '$uri4' AND id_user = '".$a['sess_konid']."'")->row();

					$detil_tes = $q_ambil_soal;
				}


				// $pc_list_jawaban = explode(",", $detil_tes->list_jawaban);

				// $arr_jawab = array();
				// foreach ($pc_list_jawaban as $v) {
				//   $pc_v = explode(":", $v);
				//   $idx = $pc_v[0];
				//   $val = $pc_v[1];

				//   $arr_jawab[$idx] = $val;
				// }

				$html = '';
				$no = 1;
				$array_urut = array("a","b","c","d","e","f","g","h","i","j");
				if (!empty($detil_tes->list_soal)) {
					$soal = unserialize($detil_tes->list_soal);
				    
				    foreach ($soal as $d) { 
				    	// $html .="-".$d['master'];
				    	$hd = explode(',', $d['master']);
				    	$nm_hd_plh = str_replace(",","_",$d['master']);
				    	$jmlhead = count($hd);

				    	$html .='<div class="col-md-6">';
	                        $html .='<table class="table table-bordered table-hover">';
	                            $html .='<tr>';
	                                $html .='<th colspan="'.$jmlhead.'" class="text-center">KOLOM '.$no.'</th>';
	                                $html .='<th>&nbsp;</th>';
	                                $html .='<th colspan="'.$jmlhead.'" class="text-center">JAWABAN</th>';
	                            $html .='</tr>';
	                            $html .='<tr>';

	                            	for ($i=0; $i < $jmlhead ; $i++) { 
	                            		$html .='<th class="text-center">'.$array_urut[$i].'</th>';
	                                }
	                                
	                                $html .='<th colspan="'.($jmlhead+1).'">&nbsp;</th>';
	                            $html .='</tr>';
	                            $html .='<tr>';
	                                
	                                for ($op=0; $op < $jmlhead; $op++) { 
	                                	$html .='<th class="text-center">'.$hd[$op].'</th>';
	                                }
	                                
	                                
	                                $html .='<th colspan="'.($jmlhead+1).'">&nbsp;</th>';
	                            $html .='</tr>';
	                            $html .='<tr>';
	                                $html .='<td colspan="'.($jmlhead+$jmlhead+1).'"></td>';
	                            $html .='</tr>';

	                            $nv = 1;
	                            foreach ($d['chil'] as $vls) {
	                            	$html .='<tr>';
		                                $html .='<td class="text-center">'.$nv.'</td>';
		                                $acks = explode(",", $vls['acak']);
		                                $nm_plh = str_replace(",","_",$vls['acak']);
		                                foreach ($acks as $ack) {
		                                	$html .='<td class="text-center">'.$ack.'</td>'; 
		                                }
		                                
		                                $html .='<th>&nbsp;</th>';
		                                for ($ll=0; $ll < $jmlhead ; $ll++) {

		                                	if($vls['jbwn']!='0'){
		                                		if($vls['jbwn']==$hd[$ll]){
		                                			$ck = "checked";
		                                		}else{
		                                			$ck = "";
		                                		}
		                                	}else{
		                                		$ck = "";
		                                	}
		                                	

		                                	$html .='<td><div class="funkyradio">
		                                	<div class="funkyradio-success">
									            <input '.$ck.' id="'.$nm_plh.'_'.$array_urut[$ll].'_id" type="radio" name="nm_'.$nm_plh.'" value="'.$hd[$ll].'" onclick="return simpansatu(\''.$hd[$ll].'\',\''.$nm_plh.'\',\''.$nm_hd_plh.'\',\''.$uri4.'\');">
									            <label for="'.$nm_plh.'_'.$array_urut[$ll].'_id">'.$array_urut[$ll].'</label>
									        </div></div>
		                                	</td>'; 
		                                	// $html .= '<div class="funkyradio-success"> <input type="radio" id="opsi_'.strtoupper($this->opsi[$j]).'_'.$d->id.'" name="opsi_'.$no.'" value="'.strtoupper($this->opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtoupper($this->opsi[$j]).'_'.$d->id.'"><div class="huruf_opsi">'.$this->opsi[$j].'</div> <p>'.$pc_pilihan_opsi[1].'</p><p>'.$tampil_media_opsi.'</p></label></div>';
		                                }
		                                
		                            $html .='</tr>';
		                            $nv ++;
	                            }
	                            
	                            
	                        $html .='</table>';
	                    $html .='</div>';
	                    
				        // $html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$d->id.'">';
				        // $html .= '<div class="step" id="widget_'.$no.'">';

				        // $html .= '<p>'.$d->soal.'</p><p>'.$tampil_media.'</p><div class="funkyradio">';

				        // for ($j = 0; $j < $this->config->item('jml_opsi'); $j++) {
				        //     $opsi = "opsi_".$this->opsi[$j];

				        //     $checked = $arr_jawab[$d->id] == strtoupper($this->opsi[$j]) ? "checked" : "";

				        //     $pc_pilihan_opsi = explode("#####", $d->$opsi);

				        //     $tampil_media_opsi = (is_file('./upload/gambar_soal/'.$pc_pilihan_opsi[0]) || $pc_pilihan_opsi[0] != "") ? tampil_media('./upload/gambar_opsi/'.$pc_pilihan_opsi[0],'auto','auto') : '';

				        //     $html .= '<div class="funkyradio-success">
				        //         <input type="radio" id="opsi_'.strtoupper($this->opsi[$j]).'_'.$d->id.'" name="opsi_'.$no.'" value="'.strtoupper($this->opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtoupper($this->opsi[$j]).'_'.$d->id.'"><div class="huruf_opsi">'.$this->opsi[$j].'</div> <p>'.$pc_pilihan_opsi[1].'</p><p>'.$tampil_media_opsi.'</p></label></div>';
				        // }
				        // $html .= '</div></div>';
				        $no++;
				    }
				}

				$a['jam_mulai'] = $detil_tes->tgl_mulai;
				$a['jam_selesai'] = $detil_tes->tgl_selesai;
				$a['id_tes'] = $cek_detil_tes->id;
				$a['no'] = $no;
				$a['html'] = $html;

				$this->load->view('v_ujian_bbb', $a);
			} else {
				redirect('mda/sudah_selesai_ujian_sp/'.$uri4);
			}
			
		}

	}

	public function sudah_selesai_ujian_sp() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		$a['detil_tes'] = $this->db->query("SELECT tr_psikotest.* 
												FROM tr_psikotest 
												WHERE tr_psikotest.id = '$uri3'")->row();

		$sqql = "
	        	SELECT a.id, b.nama, a.jumlah_benar, a.jumlah_salah, a.jumlah_non, a.time_rata, a.time_tertinggi
				FROM tr_ikut_psikotest a
				INNER JOIN m_siswa b ON a.id_user = b.id
				LEFT JOIN tr_psikotest c on c.id = a.id_psikotest
				WHERE a.id_psikotest = '$uri3' 
				AND a.id_user = '".$a['sess_konid']."'";
		$a['datanya'] = $this->db->query($sqql)->row();									

		$q_nilai = $this->db->query("SELECT tgl_selesai FROM tr_ikut_psikotest WHERE id_psikotest = '$uri3' AND id_user = '".$a['sess_konid']."' AND status = 'N'")->row();
		$show = $this->db->query("SELECT show_result FROM tr_psikotest WHERE id = '$uri3'")->row();
		// echo $show->show_result.' '.date('Y-m-d H:i:s');
		// echo "<br>";
		if($show->show_result>=date('Y-m-d H:i:s')){
			$a['show'] = 'N';
			echo "N";
		}else{
			$a['show'] = 'Y';
			echo "Y" . $uri3;
		}
		// exit;
		if (empty($q_nilai)) {
			redirect('mda/ikut_ujian_sp/_/'.$uri3);
		} else {
			$a['p'] = "v_selesai_ujian_bbb";
			$a['data'] = "<div class='alert alert-danger'>Anda telah selesai mengikuti ujian ini pada : <strong style='font-size: 16px'>".tjs($q_nilai->tgl_selesai, "l")."</strong></div>";
		}
		$this->load->view('bbb', $a);
	}

	public function h_ujian_sp() {
		$this->cek_aktif();
		
		//var def session
		$a['sess_level'] = $this->session->userdata('admin_level');
		$a['sess_user'] = $this->session->userdata('admin_user');
		$a['sess_konid'] = $this->session->userdata('admin_konid');
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);
		//var post from json
		$p = json_decode(file_get_contents('php://input'));

		if ($uri3 == "det") {
			$a['detil_tes'] = $this->db->query("SELECT  tr_psikotest.* 	FROM tr_psikotest WHERE tr_psikotest.id = '$uri4'")->row();
			

			//$a['hasil'] = $this->db->query("")->result();
			$a['p'] = "m_guru_tes_hasil_detil_bbb";
			//echo $this->db->last_query();
		}else if ($uri3 == "data_det") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("
	        	SELECT a.id
				FROM tr_ikut_psikotest a
				INNER JOIN m_siswa b ON a.id_user = b.id
				WHERE a.id_psikotest = '$uri4' 
				AND b.nama LIKE '%".$search['value']."%'")->num_rows();

	        $q_datanya = $this->db->query("
	        	SELECT a.id, b.nama, a.jumlah_benar, a.jumlah_salah, a.jumlah_non, a.time_rata, a.time_tertinggi
				FROM tr_ikut_psikotest a
				INNER JOIN m_siswa b ON a.id_user = b.id
				WHERE a.id_psikotest = '$uri4' 
				AND b.nama LIKE '%".$search['value']."%' ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();

	        $data = array();
	        $no = ($start+1);


	        foreach ($q_datanya as $d) {

	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['nama'];
	            $data_ok[2] = $d['jumlah_benar'];
	            $data_ok[3] = $d['jumlah_salah'];
	            $data_ok[4] = $d['jumlah_non'];
	            $data_ok[5] = $d['time_rata'];
	            $data_ok[6] = $d['time_tertinggi'];
	            $data_ok[7] = '<a href="'.base_url().'mda/h_ujian_sp/batalkan_ujian_sp/'.$d['id'].'/'.$this->uri->segment(4).'" class="btn btn-danger btn-xs" onclick="return confirm(\'Anda yakin...?\');"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Batalkan Ujian</a>
	            	<a href="'.base_url().'mda/evaluasi_ujian_cetak_sp/'.$d['id'].'/'.$this->uri->segment(4).'" class="btn btn-success btn-xs" target="_blank"><i class="glyphicon glyphicon-print" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Cetak Evaluasi</a>';

	            //edit button
	            //<a href="#" onclick="return h_ujian_e('.$d['id'].');" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit Hasil</a>	

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		}else if ($uri3 == "batalkan_ujian_sp") {
			$this->db->query("DELETE FROM tr_ikut_psikotest WHERE id = '$uri4'");
			redirect('mda/h_ujian_sp/det/'.$uri5);
		}else if ($uri3 == "data") {
			$start = $this->input->post('start');
	        $length = $this->input->post('length');
	        $draw = $this->input->post('draw');
	        $search = $this->input->post('search');

	        $d_total_row = $this->db->query("SELECT id FROM tr_psikotest a WHERE a.nama_ujian LIKE '%".$search['value']."%'")->num_rows();
	    	//echo $this->db->last_query();

	        $q_datanya = $this->db->query("SELECT a.* FROM tr_psikotest a WHERE a.nama_ujian LIKE '%".$search['value']."%' ORDER BY a.id DESC LIMIT ".$start.", ".$length."")->result_array();

	        $data = array();
	        $no = ($start+1);


	        foreach ($q_datanya as $d) {
	        	$gn = '';
	        	foreach(unserialize($d['deret']) as $vb){
	        		$xx = explode(',', $vb);
		        	$jl = '';
						foreach ($xx as $ke) {
						$jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
					}

	        		$gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
	        	}
	            $data_ok = array();
	            $data_ok[0] = $no++;
	            $data_ok[1] = $d['nama_ujian']."<br>Token : <b>".$d['token']."</b> &nbsp;&nbsp; <a href='#' onclick='return refresh_tokenbbb(".$d['id'].")' title='Perbarui Token'><i class='fa fa-refresh'></i></a>";
	            $data_ok[2] = $gn;
	            $data_ok[3] = $d['jumlah_kolom'];
	            $data_ok[4] = $d['jumlah_perkolom'];
	            $data_ok[5] = tjs($d['tgl_mulai'],"s")."<br>(".$d['waktu']." menit)";
	            $data_ok[6] = '<a href="'.base_url().'mda/h_ujian_sp/det/'.$d['id'].'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-search" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Lihat Hasil</a>';

	            $data[] = $data_ok;
	        }

	        $json_data = array(
	                    "draw" => $draw,
	                    "iTotalRecords" => $d_total_row,
	                    "iTotalDisplayRecords" => $d_total_row,
	                    "data" => $data
	                );
	        j($json_data);
	        exit;
		}else{
			$a['p']	= "m_guru_tes_hasil_bbb";
		}


		$this->load->view('bbb', $a);

	}
	public function hasil_ujian_cetak_sp() {
		$this->cek_aktif();
		
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$a['detil_tes'] = $this->db->query("SELECT tr_psikotest.* FROM tr_psikotest WHERE tr_psikotest.id = '$uri3'")->row();
		
		
		$a['hasil'] = $this->db->query("SELECT m_siswa.nama	FROM tr_ikut_psikotest INNER JOIN m_siswa ON tr_ikut_psikotest.id_user = m_siswa.id LEFT JOIN tr_psikotest c on c.id = tr_ikut_psikotest.id_psikotest WHERE tr_ikut_psikotest.id_psikotest = '$uri3'")->result();
		$this->load->view("m_guru_tes_hasil_detil_cetak_bbb", $a);
	}

	public function evaluasi_ujian_cetak_sp() {
		$this->cek_aktif();
		
		//var def uri segment
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);


		$a['detil_tes'] = $this->db->query("SELECT tr_psikotest.* FROM tr_psikotest 
												WHERE tr_psikotest.id = '$uri4'")->row();
		

		$a['hasil'] = $this->db->query("SELECT m_siswa.nama, tr_ikut_psikotest.jumlah_benar, tr_ikut_psikotest.jumlah_salah, tr_ikut_psikotest.jumlah_non, tr_ikut_psikotest.time_rata, tr_ikut_psikotest.time_tertinggi	FROM tr_ikut_psikotest	INNER JOIN m_siswa ON tr_ikut_psikotest.id_user = m_siswa.id WHERE tr_ikut_psikotest.id_psikotest = '$uri4' and tr_ikut_psikotest.id = '$uri3'")->result();

		$are = $this->db->query("SELECT tr_ikut_psikotest.* FROM tr_ikut_psikotest WHERE tr_ikut_psikotest.id = '$uri3'")->row();

		$a['are'] = $are;

		

		$this->load->view("v_evaluasi_ujian_cetak_bbb", $a);
	}
}