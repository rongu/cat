<!DOCTYPE html>
<html>
<head>
  <title>Evaluasi Hasil Ujian</title>
  <link href='<?php echo base_url(); ?>___/css/style_print.css' rel='stylesheet' media='' type='text/css'/>
</head>
<body>

<h3>Evaluasi Hasil Ujian</h3>
<hr style="border: solid 1px #000"><br>

<h4>Detil Ujian</h4>
<table class="table-bordered" style="margin-bottom: 0px">
  <tr><td width="30%">Mata Pelajaran</td><td><b><?php echo $detil_tes->namaMapel; ?></b></td></tr>
  <tr><td>Nama Guru</td><td width="70%"><b><?php echo $detil_tes->nama_guru; ?></b></td></tr>
  <tr><td>Nama Ujian</td><td width="70%"><b><?php echo $detil_tes->nama_ujian; ?></b></td></tr>
  <tr><td>Jumlah Soal</td><td><b><?php echo $detil_tes->jumlah_soal; ?></b></td></td></tr>
  <tr><td>Waktu</td><td><b><?php echo $detil_tes->waktu; ?> menit</b></td></tr>
  <tr><td>Tertinggi</td><td><b><?php echo $statistik->max_; ?></b></td></tr>
  <tr><td>Terendah</td><td><b><?php echo $statistik->min_; ?></b></td></tr>
  <tr><td>Rata-rata</td><td><b><?php echo number_format($statistik->avg_); ?></b></td></tr>
</table>
<br><br>
<h4>Hasil Ujian</h4>
<table class="table-bordered">
  <thead>
    <tr>
      <th width="40%">Nama Peserta</th>
      <th width="10%">Jumlah Benar</th>
      <th width="10%">Jumlah Salah</th>
      <th width="10%">Nilai</th>
      <th width="10%">Nilai Bobot</th>
      <th width="15%">Lulus/Tidak Lulus</th>
    </tr>
  </thead>

  <tbody>
    <?php 
      if (!empty($hasil)) {
        $no = 1;
        foreach ($hasil as $d) {
          echo '<tr>
                <td>'.$d->nama.'</td>
                <td class="ctr">'.$d->jml_benar.'</td>
                <td class="ctr">'.$d->jml_salah.'</td>
                <td class="ctr">'.$d->nilai.'</td>
                <td class="ctr">'.$d->nilai_bobot.'</td>
                <td class="ctr">'.$d->lulus.'</td>
                </tr>
                ';
        }
      } else {
        echo '<tr><td colspan="5">Belum ada data</td></tr>';
      }
    ?>
  </tbody>
</table>
<br>
<h4>Catatan Hasil Penilaian Test</h4>
<div  align="center"style="border:2px ridge black;color:blue;height:100px;padding-top:25px;font-size:30px;font-weight:bold;text-shadow: 2px 6px 3px black;"></div>
<br><br>
<h4>Lembar Jawaban</h4>
<table class="table-bordered">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="40%">Soal</th>
      <th width="10%">Kunci Jawaban</th>
      <th width="10%">Jawaban</th>
      <th width="10%">Hasil</th>
    </tr>
  </thead>

  <tbody>
    <?php 
      if (!empty($evaluasi)) {
        $no == 1;
        foreach ($evaluasi as $d) {
          echo '<tr>
                <td class="ctr">'.$no.'</td>
                <td>'.$d[0].'</td>
                <td class="ctr">'.$d[1].'</td>
                <td class="ctr">'.$d[2].'</td>
                <td class="ctr">'.$d[3].'</td>
                </tr>
                ';
        $no++;
        }
      } else {
        echo '<tr><td colspan="5">Belum ada data</td></tr>';
      }
    ?>
  </tbody>
</table>


</body>
</html>
