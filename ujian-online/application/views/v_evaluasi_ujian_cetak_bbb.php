<!DOCTYPE html>
<html>
<head>
  <title>Evaluasi Hasil Psikotest</title>
  <link href='<?php echo base_url(); ?>___/css/style_print.css' rel='stylesheet' media='' type='text/css'/>
</head>
<body>

<h3>Evaluasi Hasil Psikotest</h3>
<hr style="border: solid 1px #000"><br>

<h4>Detil Psikotest</h4>
<table class="table-bordered" style="margin-bottom: 0px">
  <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
  <tr><td width="30%">Deret</td><td width="70%">

    <?php 

    $gn = '';
      foreach(unserialize($detil_tes->deret) as $vb){
          $xx = explode(',', $vb);
          $jl = '';
        foreach ($xx as $ke) {
        $jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
      }

        $gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
      }
      // echo $sp_master->deret; 
      echo $gn;
      ?>            
    </td>
  </tr>
  <tr><td width="30%">Jumlah Per Kolom</td><td width="70%"><?php echo $detil_tes->jumlah_perkolom; ?></td></tr>
  <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>

</table>
<br><br>
<h4>Hasil Psikotest</h4>
<table class="table-bordered">
  <thead>
    <tr>
      <th width="40%">Nama Peserta</th>
      <th>Jumlah Benar</th>
      <th>Jumlah Salah</th>
      <th>Jumlah Tidak Terisi</th>
      <th>Waktu Rata-rata</th>
      <th>Waktu Terlama</th>
    </tr>
  </thead>

  <tbody>
    <?php 
      if (!empty($hasil)) {
        $no = 1;
        foreach ($hasil as $d) {
          echo '<tr>
                <td>'.$d->nama.'</td>
                <td>'.$d->jumlah_benar.'</td>
                <td>'.$d->jumlah_salah.'</td>
                <td>'.$d->jumlah_non.'</td>
                <td>'.$d->time_rata.'</td>
                <td>'.$d->time_tertinggi.'</td>
                </tr>
                ';
        }
      } else {
        echo '<tr><td colspan="5">Belum ada data</td></tr>';
      }
    ?>
  </tbody>
</table>
<br>
<h4>Catatan Hasil Penilaian Test</h4>
<div  align="center"style="border:2px ridge black;color:blue;height:100px;padding-top:25px;font-size:30px;font-weight:bold;text-shadow: 2px 6px 3px black;"></div>
<br><br>
<h4>Lembar Jawaban</h4>
<?php 
    $html = '';
    $no = 1;
    $array_urut = array("a","b","c","d","e","f","g","h","i","j");
    if (!empty($are->list_soal)) {
      $soal = unserialize($are->list_soal);
        
        foreach ($soal as $d) { 
          // $html .="-".$d['master'];
          $hd = explode(',', $d['master']);
          $jmlhead = count($hd);
?>          
          
                    <table class="table-bordered">
                        <tr>
                            <th colspan="<?php echo $jmlhead;?>" class="text-center">KOLOM <?php echo $no;?></th>
                            <th>&nbsp;</th>
                            <th class="text-center">KUNCI JAWABAN</th>
                            <th class="text-center">JAWABAN</th>
                            <th class="text-center">TIMER</th>
                            <th class="text-center">HASIL</th>
                        </tr>
                        <tr>
                          <?php for ($i=0; $i < $jmlhead ; $i++) { ?>
                            <th class="text-center"><?php echo $array_urut[$i];?></th>
                          <?php  } ?>
                            <th>&nbsp;</th>
                            <th colspan="4">&nbsp;</th>
                        </tr>
                        <tr>
                           <?php for ($op=0; $op < $jmlhead; $op++) { ?>
                              <th class="text-center"><?php echo $hd[$op];?></th>
                            <?php } ?>
                            <th>&nbsp;</th>
                            <th colspan="4">&nbsp;</th>
                            
                        </tr>
                        <tr>
                            <td colspan="<?php echo ($jmlhead+5);?>"></td>
                        </tr>
                        <?php
                        $nv = 1;
                        foreach ($d['chil'] as $vls) {
                        ?>
                          <tr>
                                <td style="text-align: center;"><?php echo $nv;?></td>
                                <?php
                                $acks = explode(",", $vls['acak']);
                                
                                foreach ($acks as $ack) {
                                ?>
                                  <td style="text-align: center;"><?php echo $ack;?></td> 
                                <?php } ?>                                
                                
                                <td>&nbsp;</td>
                                <td style="text-align: center;">
                                <?php

                                  $master_ps = $this->db->query("SELECT * FROM sp_master WHERE deret = '".$d['master']."'")->row();
                                  $child_ps = $this->db->query("SELECT * FROM sp_child WHERE id_sp_master = '".$master_ps->id."' and acak='".$vls['acak']."'")->row();

                                  echo $child_ps->jawaban;
                                ?>
                                </td>
                                <td style="text-align: center;">
                                  <?php
                                    echo $vls['jbwn']=="0"?"-":$vls['jbwn'];
                                  ?>
                                </td>
                                <td style="text-align: center;">
                                <?php
                                    echo $vls['tm']=="0"?"-":$vls['tm'];
                                  ?>
                                </td>
                                <td style="text-align: center;">
                                  
                                  <?php
                                  if($vls['jbwn']!="0"){
                                    if($vls['jbwn']==$child_ps->jawaban)
                                    {
                                      echo "BENAR";
                                    }else{ 
                                      echo "SALAH";
                                    }
                                  }else{
                                    "-";
                                  } 
                                   ?>

                                </td>
                                
                                
                            </tr>
                            <?php
                            $nv ++;
                        }?>
                        
                        
                    </table>
          <?php      

            $no++;
        }
    }

   
    ?>

</body>
</html>
