<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Dashboard - <?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <link rel="icon" href="<?php echo base_url(); ?>___/img/favicon.ico" type="image/gif" sizes="16x16"> 
      <link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>___/plugin/fa/css/font-awesome.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>___/plugin/datatables/dataTables.bootstrap.css" rel="stylesheet">
   </head>
   <body>

   <div class="" style="min-height: 450px">
      <nav class="navbar navbar-findcond navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand"><img src="<?php echo base_url(); ?>___/img/logo.png" style="height: 40px;" alt="" />&nbsp;<?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi'); ?></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $this->session->userdata('admin_nama')." (".$this->session->userdata('admin_user').")"; ?> <span class="caret"></span></a>
                     <ul class="dropdown-menu" role="menu">
                        <li><a href="#" onclick="return rubah_password();">Ubah Password</a></li>
                        <li><a href="<?php echo base_url(); ?>adm/logout" onclick="return confirm('keluar..?');">Logout</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </nav>


      <div class="container" style="margin-top: 70px">

         <div class="col-lg-12 row">
           <div class="panel panel-default">
             <div class="panel-body">
               <div class="row">
                        <div class="col-md-6">
                           <a href="<?php  echo base_url();?>adm/index" class="btn btn-lg btn-default btn-block" style="background-image: url('<?php echo base_url(); ?>___/img/cat.jpg'); background-size: cover; height:100px;"><span style="font-size:24.0pt;color:#52A556;position: relative;top: 25%; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black">Computer Assisted Test</span></a>
                        </div>
                        <div class="col-md-6">
                           <a href="<?php  echo base_url();?>mda/index" class="btn btn-lg btn-default btn-block"  style="background-image: url('<?php echo base_url(); ?>___/img/psikotes.jpg'); background-size: cover; height:100px;"><span style="font-size:24.0pt;color:#52A556;position: relative;top: 25%; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black">Cari Karakter Yang Hilang</a>
                        </div>
                    </div>
             </div>
           </div>
         </div>

      </div>
   
   
   </div>

   <div class="col-md-12" style="border-top: solid 4px #ddd; text-align: center; padding-top: 10px; margin-top: 50px; margin-bottom: 20px">
    &copy; 2021 <a href="<?php echo base_url(); ?>adm"><?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi')."</a> <br> Waktu Server: ".tjs(date('Y-m-d H:i:s'),"s")." - Waktu Database: ".tjs($this->waktu_sql,"s"); ?>. 
</div>

<!-- insert modal -->
<div id="tampilkan_modal"></div>


<script src="<?php echo base_url(); ?>___/js/jquery-1.11.3.min.js"></script> 
<script src="<?php echo base_url(); ?>___/js/bootstrap.js"></script>

<?php 
if ($this->uri->segment(2) == "m_soal" && $this->uri->segment(3) == "edit") {
?>
<script src="<?php echo base_url(); ?>___/plugin/ckeditor/ckeditor.js"></script>
<?php
}
?>
<!-- editor
<script src="<?php echo base_url(); ?>___/plugin/editor/nicEdit.js"></script>
 -->

<script src="<?php echo base_url(); ?>___/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>___/plugin/datatables/dataTables.bootstrap.min.js"></script>


<script src="<?php echo base_url(); ?>___/plugin/countdown/jquery.plugin.min.js"></script> 
<script src="<?php echo base_url(); ?>___/plugin/countdown/jquery.countdown.min.js"></script> 
<script src="<?php echo base_url(); ?>___/plugin/jquery_zoom/jquery.zoom.min.js"></script> 

<script type="text/javascript">
var base_url = "<?php echo base_url(); ?>";
var editor_style = "<?php echo $this->config->item('editor_style'); ?>";
var uri_js = "<?php echo $this->config->item('uri_js'); ?>";
</script>
<script src="<?php echo base_url(); ?>___/js/aplikasi.js"></script> 


</body>
</html>
