<!DOCTYPE html>
<html>
<head>
  <title>Laporan Hasil Psikotest</title>
  <link href='<?php echo base_url(); ?>___/css/style_print.css' rel='stylesheet' media='' type='text/css'/>
</head>
<body>

<h3>Laporan Hasil Psikotest</h3>
<hr style="border: solid 1px #000"><br>

<h4>Detil Psikotest</h4>

<table class="table-bordered" style="margin-bottom: 0px">
  <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
  <tr><td width="30%">Deret</td><td width="70%">

    <?php 

    $gn = '';
      foreach(unserialize($detil_tes->deret) as $vb){
          $xx = explode(',', $vb);
          $jl = '';
        foreach ($xx as $ke) {
        $jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
      }

        $gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
      }
      // echo $sp_master->deret; 
      echo $gn;
      ?>            
    </td>
  </tr>
  <tr><td width="30%">Jumlah Per Kolom</td><td width="70%"><?php echo $detil_tes->jumlah_perkolom; ?></td></tr>
  <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>

</table>
<br><br>
<h4>Hasil Psikotest</h4>
<table class="table-bordered">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="40%">Nama Peserta</th>
      
    </tr>
  </thead>

  <tbody>
    <?php 
      if (!empty($hasil)) {
        $no = 1;
        foreach ($hasil as $d) {
          echo '<tr>
                <td class="ctr">'.$no.'</td>
                <td>'.$d->nama.'</td>
                </tr>
                ';
        $no++;
        }
      } else {
        echo '<tr><td colspan="5">Belum ada data</td></tr>';
      }
    ?>
  </tbody>
</table>


</body>
</html>
