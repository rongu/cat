<?php 
$uri4 = $this->uri->segment(4);
?>

<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Daftar Hasil Tes
<!--      <div class="tombol-kanan">
        <a href='<?php echo base_url(); ?>adm/hasil_ujian_cetak/<?php echo $uri4; ?>' class='btn btn-info btn-sm' target='_blank'><i class='glyphicon glyphicon-print'></i> Cetak</a>
      </div> -->
    </div>
    <div class="panel-body">

      <div class="col-lg-12 alert alert-warning" style="margin-bottom: 20px">
        <div class="col-md-6">
            <table class="table table-bordered" style="margin-bottom: 0px">
              <tr><td>Deret</td><td>
                <?php 
                  // echo $sp_master->deret; 
                  $xx = explode(',', $sp_master->deret);
                  foreach ($xx as $ke) {
                    echo "<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
                  }
                  ?>                  
                </td></tr>
              <tr><td>Tgl</td><td><?php echo date('d M Y, H:i:s',strtotime($sp_master->tgl_input)); ?> </td></tr>
            </table>
        </div>
        <!--<div class="col-md-2"></div>-->

      </div>


      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="35%">Nilai Acak</th>
            <th width="10%">Jawaban</th>
          </tr>
        </thead>

        <tbody>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>

