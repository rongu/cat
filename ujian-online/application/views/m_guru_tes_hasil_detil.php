<?php 
$uri4 = $this->uri->segment(4);
?>

<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Daftar Hasil Tes
     <div class="tombol-kanan">
        <a href='<?php echo base_url(); ?>adm/hasil_ujian_cetak/<?php echo $uri4; ?>' class='btn btn-info btn-sm' target='_blank'><i class='glyphicon glyphicon-print'></i> Cetak</a>
      </div>
    </div>
    <div class="panel-body">

      <div class="col-lg-12 alert alert-warning" style="margin-bottom: 20px">
        <div class="col-md-6">
            <table class="table table-bordered" style="margin-bottom: 0px">
              <tr><td>Mata Kuliah</td><td><?php echo $detil_tes->namaMapel; ?></td></tr>
              <tr><td>Nama Guru</td><td><?php echo $detil_tes->nama_guru; ?></td></tr>
              <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
              <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>
            </table>
        </div>
        <!--<div class="col-md-2"></div>-->
        <div class="col-md-6">
            <table class="table table-bordered" style="margin-bottom: 0px">
              <tr><td width="30%">Jumlah Soal</td><td><?php echo $detil_tes->jumlah_soal; ?></td></tr>
              <tr><td>Tertinggi</td><td><?php echo $statistik->max_; ?></td></tr>
              <tr><td>Terendah</td><td><?php echo $statistik->min_; ?></td></tr>
              <tr><td>Rata-rata</td><td><?php echo number_format($statistik->avg_); ?></td></tr>
            </table>
        </div>
      </div>


      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="35%">Nama Peserta</th>
            <th width="10%">Jumlah Benar</th>
            <th width="10%">Jumlah Salah</th>
            <th width="10%">Nilai</th>
            <th width="10%">Nilai Bobot</th>
            <th width="15%">Lulus/Tidak</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>

        <tbody>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="e_hasil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Edit Hasil Ujian</h4>
      </div>
      <div class="modal-body">
          <form name="f_ujian" id="f_hasil" onsubmit="return h_ujian_s(<?php echo $uri4; ?>);">
            <input type="hidden" name="id" id="id" value="0">
            <input type="hidden" name="jumlah_soal1" id="jumlah_soal1" value="0">
              <table class="table table-form">
                <tr><td style="width: 25%">Nama Peserta</td><td style="width: 75%"><input type="text" class="form-control" name="nama" id="nama" required readonly></td></tr>
                <tr><td>Jumlah Benar</td><td><?php echo form_input('jml_benar', '', 'class="form-control"  id="jml_benar" required'); ?></td></tr>
                <tr><td>Jumlah Salah</td><td><?php echo form_input('jml_salah', '', 'class="form-control"  id="jml_salah" required'); ?></td></tr>
                <tr><td>Nilai</td><td><?php echo form_input('nilai', '', 'class="form-control"  id="nilai" required'); ?></td></tr>
                <tr><td>Nilai Bobot</td><td><?php echo form_input('nilai_bobot', '', 'class="form-control"  id="nilai_bobot" required'); ?></td></tr>
              </table>
              <h4>Lembar Jawaban</h4>
              <table class="table-bordered">
                <thead>
                  <tr>
                    <th width="5%">No</th>
                    <th width="40%">Soal</th>
                    <th width="10%">Kunci Jawaban</th>
                    <th width="10%">Jawaban</th>
                    <th width="10%">Hasil</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                    if (!empty($evaluasi)) {
                      $no == 1;
                      foreach ($evaluasi as $d) {
                        echo '<tr>
                              <td class="ctr">'.$no.'</td>
                              <td>'.$d[0].'</td>
                              <td class="ctr">'.$d[1].'</td>
                              <td class="ctr">'.$d[2].'</td>
                              <td class="ctr">'.$d[3].'</td>
                              </tr>
                              ';
                      $no++;
                      }
                    } else {
                      echo '<tr><td colspan="5">Belum ada data</td></tr>';
                    }
                  ?>
                </tbody>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-minus-circle"></i> Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>
