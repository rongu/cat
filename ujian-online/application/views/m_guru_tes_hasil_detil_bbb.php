<?php 
$uri4 = $this->uri->segment(4);
?>

<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Daftar Hasil Psikotest
     <div class="tombol-kanan">
        <a href='<?php echo base_url(); ?>mda/hasil_ujian_cetak_sp/<?php echo $uri4; ?>' class='btn btn-info btn-sm' target='_blank'><i class='glyphicon glyphicon-print'></i> Cetak</a>
      </div>
    </div>
    <div class="panel-body">

      <div class="col-lg-12 alert alert-warning" style="margin-bottom: 20px">
        <div class="col-md-6">
            <table class="table table-bordered" style="margin-bottom: 0px">
              <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
              <tr><td width="30%">Deret</td><td width="70%">
     
                <?php 

                $gn = '';
                  foreach(unserialize($detil_tes->deret) as $vb){
                      $xx = explode(',', $vb);
                      $jl = '';
                    foreach ($xx as $ke) {
                    $jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
                  }

                    $gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
                  }
                  // echo $sp_master->deret; 
                  echo $gn;
                  ?>            
                </td>
              </tr>
              <tr><td width="30%">Jumlah Per Kolom</td><td width="70%"><?php echo $detil_tes->jumlah_perkolom; ?></td></tr>
              <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>
            </table>
        </div>
        <!--<div class="col-md-2"></div>-->

      </div>


      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="35%">Nama Peserta</th>
            <th>Jumlah Benar</th>
            <th>Jumlah Salah</th>
            <th>Jumlah Tidak Terisi</th>
            <th>Waktu Rata-rata</th>
            <th>Waktu Terlama</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>

        <tbody>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>

