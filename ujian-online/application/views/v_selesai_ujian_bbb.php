<div class="row col-md-12 ini_bodi">
	<div class="panel panel-info">
		<div class="panel-heading">Selesai ujian   </div>
		<div class="panel-body">	
		<?php echo $data; ?>
		<div class="col-lg-12 alert" style="margin-bottom: 20px">
	        <div class="col-md-6">
	            <table class="table table-bordered" style="margin-bottom: 0px">
	              <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
	              <tr><td width="30%">Deret</td><td width="70%">
     
                <?php 

                $gn = '';
                  foreach(unserialize($detil_tes->deret) as $vb){
                      $xx = explode(',', $vb);
                      $jl = '';
                    foreach ($xx as $ke) {
                    $jl .="<span style='border: 1px solid #bdb4b4;padding: 0px 5px; margin: 0px 2px;'>".$ke."</span>";
                  }

                    $gn .= '<div style="margin-bottom: 4px;">'.$jl.'</div>';
                  }
                  // echo $sp_master->deret; 
                  echo $gn;
                  ?>            
                </td>
              </tr>
              <tr><td width="30%">Jumlah Per Kolom</td><td width="70%"><?php echo $detil_tes->jumlah_perkolom; ?></td></tr>
	              <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>
	            </table>
	        </div>
	        <!--<div class="col-md-2"></div>-->
	        
	      </div>
	      <table class="table table-bordered" id="datatabel">
	        <thead>
	          <tr>
	            <th width="40%">Nama Peserta</th>
	            <th>Jumlah Benar</th>
	            <th>Jumlah Salah</th>
	            <th>Jumlah Tidak Terisi</th>
	            <th>Waktu Rata-rata</th>
	            <th>Waktu Terlama</th>
	            
	          </tr>
	        </thead>
	        <tbody>
	        	<?php if ($show == 'Y'){?>
	        	<tr>
	        		<td><?php echo $datanya->nama; ?></td>
	        		<td><?php echo $datanya->jumlah_benar; ?></td>
	        		<td><?php echo $datanya->jumlah_salah; ?></td>
	        		<td><?php echo $datanya->jumlah_non; ?></td>
	        		<td><?php echo $datanya->time_rata; ?></td>
	        		<td><?php echo $datanya->time_tertinggi; ?></td>
	        		
	        	</tr>
	        	<?php }else{ ?>
	        	<tr>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		
	        	</tr>	
	        	<?php } ?>
	        </tbody>
	      </table>
		
		<a href="<?php echo base_url(); ?>mda/ikuti_ujian_sp">Kembali</a>
		</div>
	</div>
</div>
</div>
