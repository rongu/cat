<div class="row col-md-12 ini_bodi">
	<div class="panel panel-info">
		<div class="panel-heading">Selesai ujian   </div>
		<div class="panel-body">	
		<?php echo $data; ?>
		<div class="col-lg-12 alert" style="margin-bottom: 20px">
	        <div class="col-md-6">
	            <table class="table table-bordered" style="margin-bottom: 0px">
	              <tr><td>Mata Kuliah</td><td><?php echo $detil_tes->namaMapel; ?></td></tr>
	              <tr><td>Nama Guru</td><td><?php echo $detil_tes->nama_guru; ?></td></tr>
	              <tr><td width="30%">Nama Ujian</td><td width="70%"><?php echo $detil_tes->nama_ujian; ?></td></tr>
	              <tr><td>Waktu</td><td><?php echo $detil_tes->waktu; ?> menit</td></tr>
	            </table>
	        </div>
	        <!--<div class="col-md-2"></div>-->
	        <div class="col-md-6">
	            <table class="table table-bordered" style="margin-bottom: 0px">
	              <tr><td width="30%">Jumlah Soal</td><td><?php echo $detil_tes->jumlah_soal; ?></td></tr>
	              <?php if ($show == 'Y'){?>
		        	<tr>
		        		<tr><td>Tertinggi</td><td><?php echo $statistik->max_; ?></td></tr>
		                <tr><td>Terendah</td><td><?php echo $statistik->min_; ?></td></tr>
		                <tr><td>Rata-rata</td><td><?php echo number_format($statistik->avg_); ?></td></tr>
		        	</tr>
		        	<?php }else{ ?>
		        	<tr>
		        		<tr><td>Tertinggi</td><td></td></tr>
		                <tr><td>Terendah</td><td></td></tr>
		                <tr><td>Rata-rata</td><td></td></tr>
		        	</tr>	
		          <?php } ?>
	            </table>
	        </div>
	      </div>
	      <table class="table table-bordered" id="datatabel">
	        <thead>
	          <tr>
	            <th width="40%">Nama Peserta</th>
	            <th width="15%">Jumlah Benar</th>
	            <th width="15%">Nilai</th>
	            <th width="15%">Nilai Bobot</th>
	            <th width="15%">Lulus/Tidak</th>
	          </tr>
	        </thead>
	        <tbody>
	        	<?php if ($show == 'Y'){?>
	        	<tr>
	        		<td><?php echo $datanya->nama; ?></td>
	        		<td><?php echo $datanya->jml_benar; ?></td>
	        		<td><?php echo $datanya->nilai; ?></td>
	        		<td><?php echo $datanya->nilai_bobot; ?></td>
	        		<td><?php echo $datanya->lulus; ?></td>
	        	</tr>
	        	<?php }else{ ?>
	        	<tr>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        	</tr>	
	        	<?php } ?>
	        </tbody>
	      </table>
		
		<a href="<?php echo base_url(); ?>adm/ikuti_ujian">Kembali</a>
		</div>
	</div>
</div>
</div>
